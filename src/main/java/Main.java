import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {

    public static void main(String[] args){

        createPdf("Pawel","Pudlo","Student","2014-2018","I'm Student");

    }

    private static void  createPdf(String firstName, String lastName, String profession, String education,String summary){
        try{
            Document document = new Document();
            PdfWriter.getInstance(document,new FileOutputStream("d:/Resume.pdf"));
            document.open();

            PdfPTable  table = new PdfPTable(2);

            table.setWidthPercentage(50);
            table.setSpacingBefore(0f);
            table.setSpacingAfter(0f);

            PdfPCell cell = new PdfPCell(Phrase.getInstance("First name"));
            cell.setFixedHeight(50);
            cell.setPadding(5);
            table.addCell(cell);

            cell.setPhrase(Phrase.getInstance(firstName));
            table.addCell(cell);

            cell.setPhrase(Phrase.getInstance("Last name"));
            table.addCell(cell);
            cell.setPhrase(Phrase.getInstance(lastName));
            table.addCell(cell);

            cell.setPhrase(Phrase.getInstance("Profession"));
            table.addCell(cell);
            cell.setPhrase(Phrase.getInstance(profession));
            table.addCell(cell);

            cell.setPhrase(Phrase.getInstance("Education"));
            table.addCell(cell);
            cell.setPhrase(Phrase.getInstance(education));
            table.addCell(cell);

            cell.setPhrase(Phrase.getInstance("Summary"));
            table.addCell(cell);
            cell.setPhrase(Phrase.getInstance(summary));
            table.addCell(cell);

            document.add(table);

            document.close();

            System.out.println("Great, you create PDF");
        } catch (FileNotFoundException | DocumentException e) {
            System.out.println(e);
        }
    }

}
